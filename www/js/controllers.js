angular.module('starter.controllers', [])

.controller('AppCtrl', function ($scope, $oAuth) {

    $scope.loggedIn = $oAuth.isLoggedIn();
    //Logout Function
    $scope.logout = function() {
        $oAuth.logout();
    };
})

.controller('LoginCtrl', function ($scope, $state, $oAuth, $timeout, $cordovaSpinnerDialog, $cordovaToast, $ionicLoading) {
    $scope.loggedIn = $oAuth.isLoggedIn();
    if ($oAuth.isLoggedIn() == true) {
        $oAuth.redirectToOrder();
    }
    // Form data for the login modal
    $scope.loginData = {};
    // Perform the login action when the user submits the login form
    $scope.doLogin = function () {
        $ionicLoading.show({
            content: '<i class="icon ion-loading-a"></i>',
            animation: 'fade-in',
            showBackdrop: true
        });

        //validate login fields

        //Call the servcie to login
        $oAuth.login($scope.loginData);
     
        $timeout(function () {
            $ionicLoading.hide();
        }, 1000);
    };
    


})
.controller('ProductsCtrl', function ($scope, $oAuth) {
    $scope.loggedIn = $oAuth.isLoggedIn();
    $scope.product = {};
    $scope.newProduct = {};
    $scope.products = [
      { id: 1, name: 'Ideal Milk', price: "2.20", unit: "tin", min: "1", code: 'Milk001' },
      { id: 2, name: 'Nestle Milo', price: "10.00", unit: "tin", min: "1", code: 'Milo001' },
      { id: 3, name: 'Sardine', price: "2.50", unit: "tin", min: "1", code: 'SAR002' },
      { id: 4, name: 'Sultana Rice', price: "15.00", unit: "sack", min: "1", code: 'RICE012' },
      { id: 5, name: 'Peak Milk', price: "3.50", unit: "tin", min: "1", code: 'Milk002' },
      { id: 6, name: 'Carnation Milk', price: "2.00", unit: "tin", min: "1", code: 'Milk004' }
    ];

    $scope.keepProduct = function (prod) {
        $scope.product = prod;
    };

})
.controller('OrdersCtrl', function ($scope, $localstorage, $ionicModal, $DbOperations, $state, $ionicPopup, $ionicLoading, $timeout, $oAuth) {
    $scope.loggedIn = $oAuth.isLoggedIn();
    var orderType = "order";

    // Form data for the modal
    $scope.newOrder = {};
    $scope.orders = [];
    $scope.showDetails = false;
    getOrders();

    // Create the modal that we will use later
    $ionicModal.fromTemplateUrl('templates/addOrder.html', {
        scope: $scope
    }).then(function (modal) {
        $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeModal = function () {
        $scope.modal.hide();
    };

    // Open the login modal
    $scope.addNew = function () {
        $scope.newOrder.orderNumber = 'ORD-'+new Date().getTime();
        $scope.modal.show();
    };
    
    $scope.saveNew = function () {
        $scope.newOrder.created = new Date();
        $scope.newOrder.updated = new Date();
        $scope.newOrder.type = orderType;
        $scope.newOrder.total = 0;

        $DbOperations.saveDoc($scope.newOrder);
        getOrders();
        //finally
        $scope.modal.hide();
    };
    
    //Delete or Cancel the order
    $scope.deleteOrder = function (order) {
        //Delete the Order Here
        
        var confirmPopup = $ionicPopup.confirm({
            title: 'Delete Order',
            template: 'Are you sure you want to Delete/Cancel this order?'
        });
        confirmPopup.then(function (res) {
            if (res) {
                $DbOperations.removeDoc(order);
                getOrders();
            }
        });
    };
    
    //Open the order in a new page
    $scope.openOrder = function (order) {
        $localstorage.setObject('order', order);
        $state.go('app.order');
    };
    
    function getOrders() {
        $ionicLoading.show({
            content: '<i class="icon ion-loading-a"></i>',
            animation: 'fade-in',
            showBackdrop: true
        });
        var orders = [];
        $scope.orders = [];
        $DbOperations.getDocs().then(function (res) {
            //console.dir(res);
            res.rows.forEach(function (doc) {
                if (doc.doc.type == orderType) {
                    orders.push(doc.doc);
                }
            });
            $scope.orders = orders;
        });
        $timeout(function () {
            $ionicLoading.hide();
        }, 1000);
    };
})
.controller('OrderCtrl', function ($scope, $localstorage, $state, $DbOperations, $ionicModal, $ionicLoading, $timeout, $ionicPopup, $oAuth) {
    $scope.loggedIn = $oAuth.isLoggedIn();
    var type = "product";

    // Form data for the modal
    $scope.singleOrder = $localstorage.getObject('order');
    $scope.singleOrder.products = [];
    $scope.singleOrder.total = 0;
    $scope.newProduct = {};
    $scope.selectedProduct = null;
    
    getProducts();

    function getProducts() {
        $ionicLoading.show({
            content: '<i class="icon ion-loading-a"></i>',
            animation: 'fade-in',
            showBackdrop: true
        });
        $DbOperations.getDocs().then(function (res) {
            var prods = [];
            $scope.singleOrder.total = 0;
            res.rows.forEach(function (doc) {
                if (doc.doc.type == type && doc.doc.order == $scope.singleOrder.orderNumber) {
                    //console.log(doc.doc.cost);
                    prods.push(doc.doc);
                    $scope.singleOrder.total = parseFloat($scope.singleOrder.total) + parseFloat(doc.doc.cost);
                }
            });
            $scope.singleOrder.products = prods;
            $scope.singleOrder.total = ($scope.singleOrder.total).toFixed(2);
        });
        $timeout(function () {
            $ionicLoading.hide();
        }, 1000);
    };
    
    // Create the modal that we will use later
    $ionicModal.fromTemplateUrl('templates/addProduct.html', {
        scope: $scope
    }).then(function (modal) {
        $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeModal = function () {
        $scope.modal.hide();
    };

    // Open the login modal
    $scope.addNew = function () {
        $scope.modal.show();
    };

    $scope.addProduct = function () {
        $scope.newProduct.type = type;
        $scope.newProduct.cost = ($scope.newProduct.quantity * $scope.newProduct.product.price).toFixed(2);
        $scope.newProduct.order = $scope.singleOrder.orderNumber;

        $DbOperations.saveDoc($scope.newProduct);
        getProducts();

        //finally
        $scope.selectedProduct = null;
        $scope.modal.hide();
    };
    
    //Delete a product
    $scope.deleteProduct = function (product) {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Delete Product',
            template: 'Are you sure you want to Delete this product?'
        });
        confirmPopup.then(function (res) {
            if (res) {
                $DbOperations.removeDoc(product);
                getProducts();
            }
        });
    };
    
    //get all products
    $scope.products = [
      { id: 1, name: 'Ideal Milk', price: "2.20", unit: "tin", min: "1", code:'Milk001' },
      { id: 2, name: 'Nestle Milo', price: "10.00", unit: "tin", min: "1", code: 'Milo001' },
      { id: 3, name: 'Sardine', price: "2.50", unit: "tin", min: "1", code: 'SAR002' },
      { id: 4, name: 'Sultana Rice', price: "15.00", unit: "sack", min: "1", code: 'RICE012' },
      { id: 5, name: 'Peak Milk', price: "3.50", unit: "tin", min: "1", code: 'Milk002' },
      { id: 6, name: 'Carnation Milk', price: "2.00", unit: "tin", min: "1", code: 'Milk004' }
    ];
})

;
