angular.module('starter.services', [])

.factory('$localstorage', ['$window', function ($window) {
    return {
        set: function(key, value) {
            $window.localStorage[key] = value;
        },
        get: function(key, defaultValue) {
            return $window.localStorage[key] || defaultValue;
        },
        setObject: function(key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        },
        getObject: function(key) {
            return JSON.parse($window.localStorage[key] || '{}');
        }
    };
}])

.factory('$oAuth', function ($http, $state, $localstorage, $timeout, $ionicPopup) {

    var loggedIn = 'loggedIn';

    return {
        login: function (user) {
            $timeout(function () {
                
                if (user.username == 'admin' && user.password == 'pass') {
                    $localstorage.set(loggedIn, true);
                    $localstorage.setObject('user', user);
                    $state.go('app.activeOrders');
                } else {
                    //show an alert
                    //alert('check the login data');
                    $ionicPopup.alert({
                        title: 'Login Error',
                        content: 'Please check the login details.'
                    }).then(function (res) {

                    });
                }
                
            }, 1000);
        },

        logout: function () {
            $localstorage.set(loggedIn, false);
            $localstorage.setObject('user', {});
            $state.go('app.login');
        },

        isLoggedIn: function () {
            return $localstorage.get(loggedIn, false);
        },
        redirectToOrder: function () {
            $state.go('app.activeOrders');
        },
        redirectToLogin: function () {
            $state.go('app.login');
        }
    };

})

.factory('$DbOperations', function ($http, $location, $ionicPopup) {

    var db = new PouchDB('shopAssistTest');
    var remoteCouch = false;

    return {
        saveDoc: function (doc) {
            db.post(doc, function callback(err, result) {
                if (!err) {
                    console.dir(result);
                }
            });
        },

        getDocs: function () {
            return db.allDocs({ include_docs: true, descending: true }, function (err, doc) {
                return doc;
            });
        },
        removeDoc: function (doc) {
            //console.log(doc);
            db.remove(doc, function callback(err, result) {
                if (err) {
                    $ionicPopup.alert({
                        title: 'Delete Failed',
                        template: 'Failed to delete the record'
                    }).then(function (res) {
                        
                    });
                }
            });
        }
    };

})

;


/*.run(function($localstorage) {

  $localstorage.set('name', 'Max');
  console.log($localstorage.get('name'));
  $localstorage.setObject('post', {
    name: 'Thoughts',
    text: 'Today was a good day'
  });
  
  var post = $localstorage.getObject('post');
  console.log(post);
});*/