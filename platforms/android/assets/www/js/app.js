// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ngCordova', 'starter.controllers', 'starter.services'])

.run(function ($ionicPlatform) {
    //var db;
    $ionicPlatform.ready(function () {
        ionic.Platform.fullScreen();
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('app', {
          url: "/app",
          abstract: true,
          templateUrl: "templates/menu.html",
          controller: 'AppCtrl'
      })

      .state('app.login', {
          url: "/login",
          views: {
              'menuContent': {
                  templateUrl: "templates/login.html",
                  controller: 'LoginCtrl'
              }
          }
      })

      .state('app.settings', {
          url: "/settings",
          views: {
              'menuContent': {
                  templateUrl: "templates/settings.html"
              }
          }
      })

      .state('app.orders', {
          url: "/orders",
          views: {
              'menuContent': {
                  templateUrl: "templates/orders.html",
                  controller: 'OrdersCtrl'
              }
          }
      })

      .state('app.activeOrders', {
          url: "/activeOrders",
          views: {
              'menuContent': {
                  templateUrl: "templates/activeOrders.html",
                  controller: 'OrdersCtrl'
              }
          }
      })

      .state('app.order', {
          url: "/orders/:orderId",
          views: {
              'menuContent': {
                  templateUrl: "templates/order.html",
                  controller: 'OrderCtrl'
              }
          }
      })

      .state('app.products', {
          url: "/products",
          views: {
              'menuContent': {
                  templateUrl: "templates/products.html",
                  controller: 'ProductsCtrl'
              }
          }
      })

      .state('app.product', {
          url: "/products/:productId",
          views: {
              'menuContent': {
                  templateUrl: "templates/product.html",
                  controller: 'ProductsCtrl'
              }
          }
      });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/login');
});

